# Shopping Cart

## Context

Learn how to use `react-redux` package with your React web application.

## The Assignment

Build a simple Shopping Cart to handle basic operations amongst components.

### Setup Instructions

In Terminal:

```sh
# (1) Clone the repo
$ git clone https://gitlab.com/agzeri/react-redux-final

# (2) Go to folder
$ cd starter-code

# (3) Install dependencies
$ npm install

# (4) Run the project
$ npm start
```

## Deliverables

### Sprint 1 | Analyzing the Project

**Summary**

Once the project has been installed, analyze the code so you can see what it’s happening. Click on the buttons, and see if something is working, if not, check the code to discover the cause. **You don’t have to solve it.**

**Instructions**

No detailed steps for this part. I encourage to read the source code from the components.

**Design**

![Basic version](/demos/s1.png)

### Sprint 2 | Integrate Redux

**Summary**

In this step, you’ll need to install and configure a basic store in your project.

**Instructions**

+ Install the `redux` package
+ Create a file in `/src/` and call it `store.js`
+ Import `createStore` from redux.
+ Add a basic `reducer` that return state by default.
+ Export as default the store.

### Sprint 3 | Action Creators on Shopping Cart

**Summary**

Time to add some real interactions to our project. Let’s code both functions “Add Item to Cart” and “Remove Item from Cart”.

**Instructions**

+ Create another file in `/src/` and name it `actions.js`
+ Create one action related to “add a product to the cart” (`ADD_ITEM_TO_CART`)
  + Export the action, not as default.
+ Create an action creator `addToCart` (a function that return an object with type and payload) to handle last action type.
  + Think, what do you need to send in the payload so you can add an item to your cart?
+ Configure the reducer function to handle that action type.
  + Add a property in your initialState to handle all bikes in a cart.
  + Add a case to handle `ADD_ITEM_TO_CART` and don’t forget to import it from `actions` file
  + Modify the neeed state part
+ Now, connect your `AddItem` component to the Redux store.
  + Import your store and action type
  + Attach the dispatch to the onClick event

### Sprint 4 | Subscribe the Shopping Cart

**Summary**

Display the items added to the Shopping Cart.

**Instructions**

+ Configure the `ShoppingCart` component to consume the data added
  + You’ll need to subscribe to the Redux store.
+ Code the “Delete from Cart” event.
  + Create the action type, action creator and the reducer logic.
+ Solve the price issue in ShoppingCart, so every time you add or remove an item the price will get updated.

### Sprint 5 | React-Redux Package

**Summary**

Knowing the library bring so many advantages, we’ll see one in this step.

**Instructions**

+ Install `react-redux` npm package
+ Use `connect` method from Redux to configure your `AddItem`, `ShoppingCart` components.
+ Refactor the code, so your components now are working in the optimized way.

### Sprint 6 | Add a logger middleware

**Summary**

Middlewares are a crucial part of development. Redux has its own rules.

**Instructions**

+ Add a [logger middleware](https://github.com/LogRocket/redux-logger) to your project.
+ Import and configure `applyMiddleware` from Redux.
+ Configure your store.

### Sprint 7 | Fetching Data

**Summary**

Now, time to handle an asynchronous operations in Redux.

**API**

+ https://specializedbikes.herokuapp.com/api/v1/bikes

**Instructions**

+ Create an action to load data `FETCH_BIKES_SUCCESS`.
+ Create an action creator `fetchData`.
+ Code the reducer logic.
+ Configure your store with `redux-thunk` so it can handle these kind of operations.
+ In `Bikes` pull the data from the store.

### Sprint 8 | Adding a Loading

**Summary**

With asynchronous operations we can add a better UX interaction between our user and the page.

**Instructions**

+ Create one action: `FETCH_BIKES_BEGIN`.
+ Where do you need to run this action?
  + _HINT: You need to configure it in two places._
  + _HINT: Not on your React components._
+ Add a loading image or text, so the first time the user can see the image, after some time the user will see the whole information.

### Sprint 9 | BONUS | Combining Reducers

**Summary**

+ Let break our main reducer function into small pieces for a better maintenance.

**Instructions**

+ Use `combineReducers` to merge small reducer functions.
+ Refactor the reducers so the application can keep working.
