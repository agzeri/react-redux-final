import React, { Component } from 'react';

import Header from './components/Header/Header';
import Layout from './components/Layout/Layout';
import Bikes from './components/Bikes/Bikes';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Layout>
        <Bikes />
        <ShoppingCart />
        </Layout>
      </div>
      )
  };
}

export default App;
