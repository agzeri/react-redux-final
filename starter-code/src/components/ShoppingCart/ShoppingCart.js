import React, { Component } from 'react';

import FormatPrice from '../Utils/FormatPrice';

import './ShoppingCart.css';

class ShoppingCart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cart: []
    };
  }

  render() {
    return (
      <ul className='cart'>
        { this.state.cart.map(bike => {
          return (
            <li key={bike.id} className='item'>
              <h3 className='item__title'>{ bike.name }</h3>
              <p className='item__sku'>{ bike.sku }</p>
              <p className='item__color'>{ bike.colorName }</p>
              <p className='item__size'>{ bike.size }</p>
              <p className='item__price'>PMSM <FormatPrice price={bike.price} /></p>
              <button
                className='item__remove'>
                <i className='fa fa-trash' />
              </button>
            </li>
          );
        }) }
        <li className='total'>
          <span className='total__label'>Total</span>
          <span className='total__price'>
            <FormatPrice price={ 0 } />
          </span>
        </li>
      </ul>
    );
  }
}

export default ShoppingCart;
