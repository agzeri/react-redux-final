import React from 'react';

export default function Layout(props) {
  return (
    <div className='grid container'>
      { props.children }
    </div>
  );
}
