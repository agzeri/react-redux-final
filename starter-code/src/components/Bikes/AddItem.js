import React from 'react';

const AddItem = ({ bike }) => {
  return (
    <button
      className='bike__add'>
      <i className='fa fa-plus' />Add
    </button>
  );
}

export default AddItem;
