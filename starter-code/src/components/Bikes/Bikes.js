import React, { Component } from 'react';

import Bike from './Bike';
import AddItem from './AddItem';

import './Bikes.css';

class Bikes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bikes: [{
        "id": 1,
        "image": "https://s7d5.scene7.com/is/image/Specialized/97820-00_VENGE-SW-DISC-DI2-SAGAN-COLL-OVRX_HERO?$hybris-pdp-hero$",
        "colors": [
          "cdcdcc"
        ],
        "colorName": "Overexposed",
        "size": 52,
        "sku": "Part No. 97820-0049",
        "name": "S-Works Venge Di2 – Sagan Collection LTD",
        "price": "284310"
      },
      {
        "id": 2,
        "image": "https://s7d5.scene7.com/is/image/Specialized/97819-01_VENGE-SW-DISC-DI2-SAGAN-COLL-DKTL-CHAR_HERO?$hybris-pdp-hero$",
        "colors": [
          "a6d8ce",
          "c66861"
        ],
        "colorName": "Dark Teal/Charcoal",
        "size": 49,
        "sku": "Part No. 97819-0149",
        "name": "S-Works Venge – Sagan Collection",
        "price": "279000"
      },
      {
        "id": 3,
        "image": "https://s7d5.scene7.com/is/image/Specialized/97819-00_VENGE-SW-DISC-DI2-BLK-SILHLG_HERO?$hybris-pdp-hero$",
        "colors": [
          "2B2E34",
          "9a9b9f"
        ],
        "colorName": "Satin Black/Silver Holo/Clean",
        "size": 54,
        "sku": "Part No. 97819-0049",
        "name": "S-Works Venge",
        "price": "263300"
      }]
    };
  }

  render() {
    return (
      <div style={{ width: '80%' }}>
        <ul className='gallery'>

          { this.state.bikes.map(bike => {
            return (
              <li key={bike.id} className='gallery__item bike'>
                <AddItem bike={bike} />
                <Bike {...bike} />
              </li>
            );
          }) }
        </ul>
        <p className='pagination'>View All <i className='fa fa-long-arrow-right' /></p>
      </div>
    );
  }
}

export default Bikes;
