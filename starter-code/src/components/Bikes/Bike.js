import React from 'react';

import FormatPrice from '../Utils/FormatPrice.js';

export default function Bike(props) {
  return (
    <React.Fragment>
      <img
        className='bike__image'
        src={props.image}
        alt={props.name} />
      <ul className='bike__colors'>
        {props.colors.map((color, i) => <li key={i}><span className='circle' style={{ backgroundColor: `#${color}`}}></span></li>)}
      </ul>
      <h2 className='bike__title'>{props.name}</h2>
      <p className='bike__price'>PMSM <FormatPrice price={props.price} /> </p>
    </React.Fragment>
  )
}
